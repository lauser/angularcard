import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { UserComponent } from './user/user.component';

import { CustomMaterialModule } from './material.module';
import {FormsModule} from '@angular/forms';
import { StatusBarComponent } from './components/status-bar/status-bar.component';

import { CarRentalService } from './services/car-rental.service';
import {HttpClientModule} from '@angular/common/http';
import { AuthGuard } from './auth.guard';
import { CarComponent } from './components/car/car.component';
import { ReservationComponent } from './components/reservation/reservation.component';
import { CustomerComponent } from './components/customer/customer.component';
import { FooterComponent } from './components/footer/footer.component';
import { CarsComponent } from './pages/cars/cars.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    UserComponent,
    StatusBarComponent,
    CarComponent,
    ReservationComponent,
    CustomerComponent,
    FooterComponent,
    CarsComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CustomMaterialModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [CarRentalService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
