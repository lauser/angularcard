import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {UserComponent} from './user/user.component';
import {LoginComponent} from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { AuthGuard } from './auth.guard';
import { CustomerComponent } from './components/customer/customer.component';
import { CarsComponent } from './pages/cars/cars.component';

const routes: Routes = [
  { path: 'user', component: UserComponent, canActivate:[AuthGuard]},
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent, canActivate:[AuthGuard] },
  { path : 'customer', component: CustomerComponent},
  { path : '', component : LoginComponent},
  {path: 'home/cars', component: CarsComponent},
  {path: 'home/customer', component: CustomerComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }