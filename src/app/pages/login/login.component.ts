import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {Router} from '@angular/router';
import { AppSettings } from '../../appConfig';

class Login {
  constructor(public userName: string = '',
    public pass: string = '') { }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  title='Car Rental';
  today: number = Date.now();
  model: Login = new Login();
  @ViewChild('f') form: any;
  @ViewChild('divMessage') divM: ElementRef;

  constructor(private router: Router) { }

  ngOnInit() {
  }


  login() {
    var arr = [];
    arr = AppSettings.credentials.login;
    var usr = arr.filter(x => x.user === this.form.value.user && x.pass === this.form.value.password);

    if (usr === undefined || usr.length == 0) {
      this.divM.nativeElement.setAttribute("style", "display:''");
    } else {    
      localStorage.setItem('token','something'); 
      localStorage.setItem('user', this.form.value.user); 
      this.router.navigate(['home']);
      
    }

  }

}
