import { Component, OnInit } from '@angular/core';
import { CarRentalService } from 'src/app/services/car-rental.service';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {

  cars = [];
  constructor(private carRental: CarRentalService) { }

  ngOnInit() {

    this.carRental.getCars().subscribe(
      res => { this.cars = res },
      err => { console.log(err) }
    )
  
  }

}
