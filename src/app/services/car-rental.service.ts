import { Injectable } from '@angular/core';
import {HttpClient}from '@angular/common/http';
import { Observable } from '../../../node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class CarRentalService {
  API_URI = '../../assets';
  private headers: Headers;
  
  constructor(private http: HttpClient) { 
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
  }
  
  getCars(): Observable<any>{

    return this.http.get(`${this.API_URI}/carRental.json`);
 
  }

  loggedIn(){

    return !!localStorage.getItem('token');

  }
}
