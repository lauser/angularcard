import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../../services/data.service'

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {

  @Input() carInfo ;
  
  // message:string;
  constructor(private data: DataService) { }
  
  ngOnInit() {
    // this.data.currentMessage.subscribe(message => this.message = message);
    // console.log(this.carInfo);
  }

  newMessage(){
    this.data.changeMessage(this.carInfo.historyReservation)
  }

}
