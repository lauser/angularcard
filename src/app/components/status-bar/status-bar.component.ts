import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-status-bar',
  templateUrl: './status-bar.component.html',
  styleUrls: ['./status-bar.component.css']
})
export class StatusBarComponent implements OnInit {

  title='Car Rental';
  today: number = Date.now();
  user : string;
  hide : boolean = false;
  constructor(private router: Router) { }

  ngOnInit() {
    this.user = localStorage.getItem('user');
    if(this.user) {
      this.hide = true;
    } 
 
  }

  logOut(){
    localStorage.clear();
    this.router.navigate(['']);
  }

}


